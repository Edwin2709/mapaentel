var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
const session = require('express-session');
const flash = require('express-flash');
const passport = require('passport');
const methodOverride = require('method-override');
const bodyParser = require('body-parser');
const favicon = require('serve-favicon');

var app = express();
var sessionStore = new session.MemoryStore;

require('./Config/passport');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var testrouter = require('./routes/test');

// view engine setup
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'pug');



app.use(bodyParser.urlencoded({ extended:true }));
app.use(bodyParser.json());

app.use(methodOverride('_method'));


app.use(cookieParser('keyboard cat'));
app.use(session({
  secret: '_secret_',
  resave: false,
  saveUninitialized: false,
  cookie: { maxAge: 1000*60*60*3 },
  store: sessionStore
}));
app.use(flash());

app.use(passport.initialize());
app.use(passport.session());

app.use(function(req, res, next){
  res.locals.sessionFlash = req.session.sessionFlash;
  res.locals.user = req.user || null;
  res.locals.error = req.flash('error');
  delete req.session.sessionFlash;
  next();
  });



app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/test', testrouter);

app.use(require('./routes/index'));
app.use(require('./routes/test'));
app.use(require('./routes/users'));

app.get('/', (req,res)=>{
  req.flash('success_msg', 'Redireccionado...');
  res.redirect('/login');  
});



// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
