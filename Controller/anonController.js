const passport = require("passport");

exports.loginGet = (req, res) => {
  res.render('login')};

exports.loginPost = (req, res, next) => {
  console.log('uuuuuuuu')
  passport.authenticate('local', {
		successRedirect: '/test',
		failureRedirect: '/login',
    failureFlash: true,
    successFlash: true
	})(req, res, next);
}
